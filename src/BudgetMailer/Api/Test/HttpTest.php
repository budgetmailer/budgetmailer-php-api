<?php

/**
 * BudgetMailer PHP API (https://www.budgetmailer.nl/index.php)
 * 
 * @author    BudgetMailer <info@budgetmailer.nl>
 * @copyright (c) 2015 - 2017 - BudgetMailer
 * @license   https://gitlab.com/budgetmailer/budgetmailer-php-api/blob/master/LICENSE.txt
 * @package   BudgetMailer\API\Client
 * @version   1.0.2
 */

/**
 * Namespace
 *
 * @package BudgetMailer\Api\Test
 */
namespace BudgetMailer\Api\Test;

use BudgetMailer\Api\Config;
use BudgetMailer\Api\Client\Http;

class HttpTest extends \PHPUnit_Framework_TestCase
{
    const CLS_HTTP = 'BudgetMailer\Api\Client\Http';
    
    protected $config;
    protected $configData;
    protected $configFile;
    protected $http;
    
    public function setUp()
    {
        $this->configFile = BM_ROOT . 'build/tests/config.php';
        
        if (!is_readable($this->configFile)) {
            throw new \Exception('Example config file not found.');
        }
        
        $this->configData = include $this->configFile;
        
        if (!is_array($this->configData) || !count($this->configData)) {
            throw new \Exception('Example configuration is empty.');
        }
        
        $this->config = new Config($this->configData);
        
        $this->http = new Http($this->config);
    }
    
    public function testHttp()
    {
        $this->assertTrue(
            self::CLS_HTTP == get_class($this->http)
        );
    }
    
    public function testGet()
    {
        $this->http->get('https://en.wikipedia.org/wiki/Main_Page');
        
        $this->assertEquals(Http::OK, $this->http->getResponseCode());
        $this->assertNotEmpty($this->http->getResponseBody());
        $this->assertNotEmpty($this->http->getResponseMessage());
    }
    
    /**
     * @expectedException RuntimeException
     */
    public function testNonExistantDomain()
    {
        $this->http->get('http://asdfjoweijfoweifjwe.com');
        
        $this->assertTrue(empty($this->http->getResponseBody()));
        $this->assertTrue(empty($this->http->getResponseCode()));
        $this->assertTrue(empty($this->http->getResponseMessage()));
    }
}
