<?php

/**
 * BudgetMailer PHP API (https://www.budgetmailer.nl/index.php)
 * 
 * @author    BudgetMailer <info@budgetmailer.nl>
 * @copyright (c) 2015 - 2017 - BudgetMailer
 * @license   https://gitlab.com/budgetmailer/budgetmailer-php-api/blob/master/LICENSE.txt
 * @package   BudgetMailer\API\Client
 * @version   1.0.2
 */

/**
 * Namespace
 *
 * @package BudgetMailer\Api\Test
 */
namespace BudgetMailer\Api\Test;

use BudgetMailer\Api\Config;

class ConfigTest extends \PHPUnit_Framework_TestCase
{
    const CLS_CONFIG = 'BudgetMailer\Api\Config';
    
    protected $config;
    protected $configData;
    protected $configFile;
    
    public function setUp()
    {
        $this->configFile = BM_ROOT . 'build/tests/config.php';
        
        if (!is_readable($this->configFile)) {
            throw new \Exception('Example config file not found.');
        }
        
        $this->configData = include $this->configFile;
        
        if (!is_array($this->configData) || !count($this->configData)) {
            throw new \Exception('Example configuration is empty.');
        }
        
        $this->config = new Config($this->configData);
    }
    
    public function testConfigData()
    {
        $this->assertTrue(
            self::CLS_CONFIG == get_class($this->config)
        );
        
        foreach ($this->configData as $k => $v) {
            $getter = 'get' . ucfirst($k);
            
            $this->assertTrue(
                $v == $this->config->$getter()
            );
            
            $this->assertTrue(
                $v == $this->config->$k
            );
        }
    }
}
