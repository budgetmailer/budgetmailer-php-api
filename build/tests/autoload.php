<?php

/**
 * BudgetMailer PHP API (https://www.budgetmailer.nl/index.php)
 * 
 * @author BudgetMailer <info@budgetmailer.nl>
 * @copyright (c) 2015 - 2017 - BudgetMailer
 * @license https://gitlab.com/budgetmailer/budgetmailer-php-api/blob/master/LICENSE.txt
 * @package BudgetMailer\API\Client
 * @version 1.0.2
 */

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once __DIR__ . '/AutoloaderPsr4.php';

define('BM_ROOT', realpath(__DIR__ . '/../../') . '/');

$loader = new AutoloaderPsr4;
$loader->register();
$loader->addNamespace('BudgetMailer\Api', BM_ROOT . 'src/BudgetMailer/Api');
