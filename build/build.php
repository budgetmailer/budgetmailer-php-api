<?php

/**
 * BudgetMailer PHP API (https://www.budgetmailer.nl/index.php)
 * 
 * @author BudgetMailer <info@budgetmailer.nl>
 * @copyright (c) 2015 - 2017 - BudgetMailer
 * @license https://gitlab.com/budgetmailer/budgetmailer-php-api/blob/master/LICENSE.txt
 * @package BudgetMailer\API\Client
 * @version 1.0.2
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

$target = isset($_SERVER['argv']) && isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : 'all';

if ('tests' == $target || 'all' == $target) {
    print PHP_EOL . 'Testing:' . PHP_EOL . PHP_EOL;
    
    if (!bm_tests()) {
        print PHP_EOL . 'Testing failed.' . PHP_EOL;
        exit(10);
    }
    
    print PHP_EOL;
}

if ('phpcpd' == $target || 'qa' == $target || 'all' == $target) {
    print PHP_EOL . 'Quality Assurance phpcpd:' . PHP_EOL . PHP_EOL;
    
    if (!bm_qa_phpcpd()) {
        print PHP_EOL . 'phpcpd failed.' . PHP_EOL;
        exit(20);
    }
    
    print PHP_EOL;
}

if ('phpcs' == $target || 'qa' == $target || 'all' == $target) {
    print PHP_EOL . 'Quality Assurance phpcs:' . PHP_EOL . PHP_EOL;
    
    if (!bm_qa_phpcs()) {
        print PHP_EOL . 'phpcs failed.' . PHP_EOL;
        exit(21);
    }
    
    print PHP_EOL;
}

if ('phpmd' == $target || 'qa' == $target || 'all' == $target) {
    print PHP_EOL . 'Quality Assurance phpmd:' . PHP_EOL . PHP_EOL;
    
    if (!bm_qa_phpmd()) {
        print PHP_EOL . 'phpmd failed.' . PHP_EOL;
        exit(22);
    }
    
    print PHP_EOL;
}

if ('docs' == $target || 'all' == $target) {
    print PHP_EOL . 'Documentation:' . PHP_EOL . PHP_EOL;
    
    if (!bm_docs()) {
        print PHP_EOL . 'Documentation failed.' . PHP_EOL;
        exit(30);
    }
    
    print PHP_EOL;
}

if ('dist' == $target || 'all' == $target) {
    print PHP_EOL . 'Single file distribution:' . PHP_EOL . PHP_EOL;
    
    if (!bm_dist()) {
        print PHP_EOL . 'Single file distribution failed.' . PHP_EOL;
        exit(40);
    }
    
    print PHP_EOL;
}

print 'Done' . PHP_EOL;

function bm_dist() {
    $distRoot = realpath(__DIR__ . '/dist') . '/';
    $srcRoot = realpath(__DIR__ . '/../src/BudgetMailer/Api') . '/';

    if (!is_dir($distRoot)) {
        print 'Dist root not found.' . PHP_EOL;
        return false;
    }

    if (!is_dir($srcRoot)) {
        print 'Source root not found.' . PHP_EOL;
        return false;
    }

    $dist = '<?php' . PHP_EOL;
    $it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($srcRoot));
    $it->rewind();

    while($it->valid()) {
        if (!$it->isDot() && !strstr($it->getSubPathName(), 'Test/')) {
            $contents = '#' . PHP_EOL . '# FILE: ' . $it->getSubPathName() . PHP_EOL . '#' . PHP_EOL . PHP_EOL;
            $contents .= file_get_contents($it->key());

            if (!$contents) {
                print 'File not readable.' . PHP_EOL;
                return false;
            }

            $dist .= str_replace('<?php' . PHP_EOL, '', $contents) . PHP_EOL . PHP_EOL;
        }

        $it->next();
    }

    if (!file_put_contents($distRoot . 'budgetmailer-php-api.php', $dist)) {
        print 'Could not write the output.' . PHP_EOL;
        return false;
    }
    
    print 'Single file distribution build successfull.' . PHP_EOL;
    
    return true;
}

function bm_docs() {
    $out = $return = null;
    
    exec('phpdoc -c build/docs/phpdoc.xml --force', $out, $return);
    
    print implode(PHP_EOL, $out);
    
    return $return === 0;
}

function bm_qa_phpcpd() {
    $out = $return = null;
    
    exec('phpcpd src/', $out, $return);
    
    print implode(PHP_EOL, $out);
    
    return $return === 0;
}

function bm_qa_phpcs() {
    $out = $return = null;
    
    exec('phpcs --standard=PSR2 src/BudgetMailer/Api/*.php src/BudgetMailer/Api/Client/*.php', $out, $return);
    
    print implode(PHP_EOL, $out);
    
    return $return === 0;
}

function bm_qa_phpmd() {
    $out = $return = null;
    
    exec('phpmd src/BudgetMailer/Api/Cache.php,src/BudgetMailer/Api/Client/Http.php,src/BudgetMailer/Api/Client/RestJson.php,src/BudgetMailer/Api/Client.php,src/BudgetMailer/Api/Config.php text cleancode,codesize,controversial,design,naming,unusedcode', $out, $return);

    $text = implode(PHP_EOL, $out);
    
    print $text;
    
    file_put_contents('build/docs/phpmd.txt', $text);
    
    return true;
}

function bm_tests() {
    $out = $return = null;
    
    exec('phpunit -c build/tests/phpunit.xml', $out, $return);
    
    print implode(PHP_EOL, $out);
    
    return $return === 0;
}

